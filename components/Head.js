const charset = document.createElement('meta')
charset.setAttribute('charset', 'UTF-8')

const viewport = document.createElement('meta')
viewport.setAttribute('name', 'viewport')
viewport.setAttribute('content', 'width=device-width, initial-scale=1.0')

const link = document.createElement('link')
link.setAttribute('rel', 'stylesheet')
link.setAttribute('href', 'css/style.css')

const title = document.createElement('title')
title.innerHTML = `Currency Exchange SPA`

document.head.appendChild(charset)
document.head.appendChild(viewport)
document.head.appendChild(link)
document.head.appendChild(title)


