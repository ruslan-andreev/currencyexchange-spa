class Currency{
    constructor(){
        this.arrCurrency = [];
    }
    getCurrencyData(){
        this.arrCurrency = JSON.parse(localStorage.getItem('apiData'))
        //console.log(this.arrCurrency)

        this.arrSelected = this.arrCurrency.filter(item =>{
            return item.Cur_Abbreviation == "USD" || item.Cur_Abbreviation == "EUR" || item.Cur_Abbreviation == "RUB"
            
        })
        localStorage.setItem('arrSelect', JSON.stringify(this.arrSelected))
        //console.log(this.arrSelected)
        
        return this.create()
    }
    create(){
        this.element = document.createElement('div')
        this.element.classList.add('currencies')

        this.list = ''
        this.arrSelected.forEach(item =>{
            
            this.list += `<li>${item.Cur_Abbreviation} : ${item.Cur_Scale} ${item.Cur_Name} - ${item.Cur_OfficialRate.toFixed(2)} (BYN)</li>`
            //console.log(this.list)
        })
        
        this.element.innerHTML = `
            <div class="currency__list">
                <ul>${this.list}</ul>
                <button class="calc__btn">Converter</button>
            </div>    
        `
        //console.log(this.element)
        return this.element
    }
    init(){
        return this.getCurrencyData()
    }
}
const currency = new Currency().init()
export default currency;