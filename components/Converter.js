class Converter{
    constructor(){
        this.convertData = []
    }
    getData(){
        this.convertData = JSON.parse(localStorage.getItem('arrSelect'))
        //console.log(this.convertData)
        this.Usd = this.convertData.find(item =>{
            return item.Cur_Abbreviation == "USD"})
            //console.log(this.Usd.Cur_OfficialRate)
        
        this.Eur = this.convertData.find(item =>{
            return item.Cur_Abbreviation == "EUR"})
            //console.log(this.Eur.Cur_OfficialRate) 
            
        this.Rub = this.convertData.find(item =>{
            return item.Cur_Abbreviation == "RUB"})
            //console.log(this.Rub.Cur_OfficialRate)
        
        console.log(JSON.parse(localStorage.getItem('inputValue')))    
        this.inputValue  = JSON.parse(localStorage.getItem('inputValue'))
        
        return this.create()
    }
    create(){

        let calcUsd= this.inputValue / this.Usd.Cur_OfficialRate;
        //console.log(calcUsd)
        let calcEur= this.inputValue / this.Eur.Cur_OfficialRate;
        let calcRub= (this.inputValue /this.Rub.Cur_OfficialRate)*100;

        this.show = document.querySelector('.converted__value')
        if(this.show) this.show.innerHTML='';

        this.element = document.createElement('ul')
        this.element.classList.add('converted__list')
        console.log(this.inputValue)
        if(this.inputValue <= 0 || this.inputValue == null){
            this.element.innerHTML='Вы ввели не верное значение'
           return this.element
        }else{this.element.innerHTML = `
            
            <li class="converted__item">USD - ${calcUsd.toFixed(2)}</li>
            <li class="converted__item">EUR - ${calcEur.toFixed(2)}</li>   
            <li class="converted__item">RUB - ${calcRub.toFixed(2)}</li>
        `
        return this.element}
        
        /*this.element.innerHTML = `
            
                <li class="converted__item">USD - ${calcUsd}</li>
                <li class="converted__item">EUR - ${calcEur}</li>   
                <li class="converted__item">RUB - ${calcRub}</li>
        `
        return this.element*/

    }
    init(){
        return this.getData()
    }
}
const converter = new Converter()
console.log(new Converter().init)
export default converter;