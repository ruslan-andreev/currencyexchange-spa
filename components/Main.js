class Main{

    create(){
        this.element = document.createElement('main')
        this.element.classList.add('main')

        import ('./Currency.js')
        .then(data =>{
            //console.log(data)
            let moduleCurrency = data.default
            //console.log(moduleCurrency)
            this.element.appendChild(moduleCurrency)

            let btn = document.querySelector('.calc__btn')
            //console.log(btn)
            if(btn){
                btn.addEventListener('click',event =>{
                    //console.log("Btn is OK!")
                    import ('./PopUp.js')
                    .then(data =>{
                        //console.log(data)
                        let modulePopUp = data.default
                        //console.log(modulePopUp)
                        let appPopUp = document.querySelector(".app")
                        appPopUp.appendChild(modulePopUp)

                        let btnClose = document.querySelector('.popup__close')
                        //console.log(btnClose)
                        btnClose.addEventListener('click',(event)=>{
                        let popup = event.target.parentElement.parentElement.parentElement

                        //после закрытия PopUp очистить поле ввода ирезультат
                        
                        this.show = document.querySelector('.converted__value')
                        if(this.show) this.show.innerHTML='';
                        popup.remove()

                        })


                        //поле ввода и кнопка конвертора
                        let convertInput = document.querySelector('.input__field')
                        //console.log(convertInput)
                        convertInput.addEventListener('keyup', event =>{
                            if (event.code.includes('Enter')){ //содержится ли слово в строке инклюдс
                                let valueByn = +event.target.value
                               //console.log(valueByn)
                                
                               localStorage.setItem('inputValue', JSON.stringify(valueByn))
                               //console.log(localStorage.getItem('inputValue'))

                                import ('./Converter.js')
                                .then(data =>{
                                    //console.log(data)
                                   
                                    let moduleConverted = data.default.init()
                                    
                                    //console.log(moduleConverted)
                                    let convertedValue = document.querySelector('.converted__value')
                                    convertedValue.appendChild(moduleConverted) 
            
                                })
                            }   
                        })

                        
                        let convertBtn = document.querySelector('.convert__btn')
                        //console.log(convertBtn)
                        convertBtn.addEventListener('click',event =>{
                            let valueByn = +document.querySelector('.input__field').value
                            //console.log(valueByn)
                            localStorage.setItem('inputValue', JSON.stringify(valueByn))
                               //console.log(localStorage.getItem('inputValue'))
                            import ('./Converter.js')
                            .then(data =>{
                                //console.log(data)
                                let moduleConverted = data.default.init()
                                //console.log(moduleConverted)
                                let convertedValue = document.querySelector('.converted__value')
                                convertedValue.appendChild(moduleConverted)
                            })

                        })    

                    })
                })
            }
            //console.log(btn)
        })

        return this.element
        
    }

    init(){
        return this.create()
    }
}
const main = new Main().init()
export default main;