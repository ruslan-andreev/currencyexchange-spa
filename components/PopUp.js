class PopUp{

    create(){
        this.element = document.createElement('div')
        this.element.classList.add('popup')

        this.element.innerHTML = `
            <div class="popup__window">
                <div class="popup__header">
                    <h2>Currency Converter</h2>
                    <button class="popup__close">Close</button>
                </div>
                <div class="popup__main">
                    <h2>Enter BYN Value</h2>
                    <input tipe="text" class="input__field"></input>
                    <div class="converted__value"></div>
                    <button class="convert__btn">Calculate</button>
                </div>
                  
            <div>
        
        `
        return this.element 
    }
    
    init() {
        return this.create()  
    }
}
const popUp = new PopUp().init();
export default popUp;
