class Data{
    async getData(){
      return  await fetch('https://www.nbrb.by/api/exrates/rates?periodicity=0')
                .then(resp =>resp.json())
                .then(data =>{
                    
                    localStorage.setItem('apiData', JSON.stringify(data))
                    //console.log(data)
                    return data
                })
                

    }
    init(){
        return this.getData()
    }
}
const data = new Data().init();
export default data;