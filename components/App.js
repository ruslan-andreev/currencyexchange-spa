class App{
    create(){
        this.app = document.createElement('div')
        this.app.classList.add('app')

        document.body.appendChild(this.app)
    }
    init(){
        import ('./Head.js').then(() =>{
            import ('./Data.js').then((data)=>{
                    data.default.then((modulData) => {
                    localStorage.setItem('apiData', JSON.stringify(modulData))    
                        //console.log(JSON.parse(localStorage.getItem('apiData')))

                        this.create()

                        import ('./Header.js')
                        .then(header =>{
                            let moduleHeader = header.default
                            this.app.appendChild(moduleHeader)

                            import ('./Main.js')
                            .then(main =>{
                                let moduleMain = main.default
                                this.app.appendChild(moduleMain)

                                import ('./Footer.js')
                                    .then(footer=>{
                                        let moduleFooter = footer.default
                                        this.app.appendChild(moduleFooter)
                                    })
                            }) 
                        })  
                    })     
            })
    })
    }
}
const app = new App().init();
export default app;