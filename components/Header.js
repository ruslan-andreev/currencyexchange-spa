class Header{
    constructor(){
        this.date = new Date();
        this.opt ={
            year:'numeric',
            month: 'numeric',
            day: 'numeric'
        }
    }
    create(){
        this.element = document.createElement('header')
        this.element.classList.add('header')
        this.element.innerHTML = `
            <div class="container">
                <div class="header__content">
                    <h1>Currency Exchange</h1>
                    <p>On Date</p>
                    <div class="header__date">${this.date.toLocaleString('ru',this.opt)}</div>
                    
                </div>
                <hr>
            </div>
        `
        return this.element
    }
    init(){
        return this.create()
    }
}
const header = new Header().init()
export default header;