class Footer{

    create(){
        this.element = document.createElement('footer')
        this.element.classList.add('footer')
        this.element.innerHTML =`
            <div class="footer">
                <div class="container">
                    <hr>
                    <p class="footer__text">&copy Ruslan Andreeev 2021</p>
                </div>
            </div>
        `
        return this.element
    }
    init(){
        return this.create()
    }
}
const footer = new Footer().init()
export default footer;